'use client';
import './style.css';
import ICArrowRight2 from '@/components/Icons/ICArrowRight2';
import Image from 'next/image';
import SlideProductComponent from '@/components/component-ui-custom/slide-swiper-product/slide-product';
import { IItemAttributeProduct } from '@/types/types-general';
import map from 'lodash.map';
import ItemMobile from '@/components/component-ui-custom/item-product-mobile';
import React from 'react';
import Link from 'next/link';
import ProductListFilter from '../ProductListFilter';
import { useState, useRef, useEffect } from 'react';

interface IProps {
  slug?: any;
  dataListAttribute: IItemAttributeProduct[];
  dataListProductInit: any;
  searchParams?: any;
  listAttributeNew?: any;
  dataAcf?: any;
  // dataListProductInit?: any;
}

export default function ListProductCategory(props: IProps) {
  const {
    slug,
    dataListAttribute,
    dataListProductInit,
    searchParams,
    listAttributeNew,
    dataAcf,
    // dataListProductInit,
  } = props;
  const [isExpanded, setIsExpanded] = useState(false);
  const [descriptionHeight, setDescriptionHeight] = useState('10rem');
  const descriptionRef = useRef<HTMLDivElement | null>(null);

  useEffect(() => {
    if (isExpanded && descriptionRef.current) {
      setDescriptionHeight(`${descriptionRef.current.scrollHeight}px`);
    } else {
      if (window) {
        window?.innerWidth < 768
          ? setDescriptionHeight('20rem')
          : setDescriptionHeight('10rem');
      }
    }
  }, [isExpanded]);

  const toggleDescription = () => {
    setIsExpanded(!isExpanded);
  };

  return (
    <div className="list-product-container mb-[2.94rem]">
      {/* banner */}
      <div className="height-banner-global relative bg-banner-about-us bg-cover bg-no-repeat w-full">
        <Image
          src={
            dataListProductInit?.image_banner?.url ||
            '/img/about-us/bg-banner-about-us.jpg'
          }
          width={1600}
          height={1000}
          alt="banner cart"
          className="w-full h-full object-cover"
          priority
        />
        <div className="absolute bottom-20 left-[8rem]">
          {/* <h1 className="text-white text-[3.125rem] leading-[4.6875rem] font-bold not-italic max-md:hidden max-md:font-bold max-md:text-[4.8rem] max-md:leading-[7.2rem]">
            LỜI CẢM ƠN
          </h1> */}
          <div className="flex items-center">
            <Link
              href={'/'}
              className="text-white text-[0.875rem] font-semibold leading-[2.25rem] not-italic max-md:text-[3.2rem]"
            >
              Trang chủ
            </Link>
            <div className="bg-[#81C8C2] h-[0.625rem] w-[0.625rem] rounded-full mx-[1rem] max-md:w-[2.13333rem] max-md:h-[2.13333rem] max-md:mx-[2rem]" />
            <span className="text-white text-[0.875rem] font-semibold leading-[2.25rem] not-italic max-md:text-[3.2rem] max-md:leading-[4.8rem]">
              Danh mục sản phẩm
            </span>
            <div className="bg-[#81C8C2] h-[0.625rem] w-[0.625rem] rounded-full mx-[1rem] max-md:w-[2.13333rem] max-md:h-[2.13333rem] max-md:mx-[2rem]" />
            <span className="text-white text-[0.875rem] font-semibold leading-[2.25rem] not-italic max-md:text-[3.2rem] max-md:leading-[4.8rem]">
              {dataListProductInit?.category_name}
            </span>
          </div>
        </div>
      </div>

      {/* content */}
      <div className="w-[87.5rem] mx-auto mt-[2.5rem] max-md:w-full max-md:px-[3.2rem] max-md:mt-[3.2rem]">
        <ProductListFilter
          dataListInit={dataListProductInit}
          listAttribute={dataListAttribute}
          searchParams={searchParams}
          listAttributeNew={listAttributeNew}
          datafilter={dataListProductInit?.bo_loc}
          slug={slug}
          type="category"
        />

        <div className="description-category">
          {/* <h3 className="text-center font-bold text-[2.5rem]">Mô tả</h3> */}
          <div className="content-desc-cate relative max-md:mt-16 mt-8">
            <div
              ref={descriptionRef}
              dangerouslySetInnerHTML={{
                __html: dataListProductInit?.description,
              }}
              className={`[&>h2]:font-bold [&>h2]:text-[2rem] transition-all duration-500 [&>h3]:font-bold [&>h3]:text-[1.5rem] [&>h4]:font-bold [&>h4]:text-[1.25rem] [&>h5]:font-bold [&>h5]:text-[1rem] [&>h6]:font-bold [&>h6]:text-[0.875rem] max-md:text-[3rem] max-md:[&>h2]:text-[4rem] max-md:[&>h3]:text-[3.5rem] max-md:[&>h4]:text-[3rem] max-md:[&>h5]:text-[2.5rem] max-md:[&>h6]:text-[2rem]`}
              style={{
                // height: isExpanded ? descriptionHeight : '10rem',
                height: descriptionHeight,
                overflow: isExpanded ? 'visible' : 'hidden',
              }}
            ></div>

            <div
              className={`overlay-desc-cate bg-[linear-gradient(0deg,rgba(255,255,255,1)49%,rgba(255,255,255,0)100%);] absolute bottom-0 left-0 w-full h-[30%] z-10 ${
                isExpanded ? 'hidden' : 'block'
              }`}
            ></div>
          </div>
          <div
            className="text-center flex justify-center max-md:text-[3rem] cursor-pointer"
            onClick={toggleDescription}
          >
            {isExpanded ? 'Ẩn bớt -' : 'Xem thêm +'}
          </div>
        </div>
        <div className="mb-[5rem] max-lg:mx-[3.25rem] mt-[3.75rem] relative max-md:mx-0 max-md:mb-[3.5rem] max-md:mt-[8.53rem]">
          <div className="flex justify-between mb-[2rem] items-center max-md:px-0 max-md:mb-[4.27rem]">
            <h2 className="text-[2rem] not-italic font-extrabold text-[#313131] leading-[2.4rem] h-[2.4rem] text-center max-md:text-[4.7rem]">
              SẢN PHẨM NỔI BẬT
            </h2>
          </div>
          <div className="w-full max-md:hidden">
            {/* <OutstandingProduct /> */}
            {dataListProductInit && dataListProductInit?.data?.length > 0 ? (
              <SlideProductComponent
                keySlide="out-standing-product"
                breakPoint={{ PerView767: 2 }}
                data={dataListProductInit?.data}
              />
            ) : (
              <div className="w-full flex justify-center">
                <Image
                  src="/img/no-data.svg"
                  alt="banner-aboutus"
                  height={300}
                  width={300}
                />
              </div>
            )}
          </div>

          <div className="hidden max-md:flex w-full overflow-x-auto hide-scrollbar-global">
            {dataListProductInit && dataListProductInit?.data?.length > 0 ? (
              map(dataListProductInit?.data, (item, index) => (
                <div className="min-w-[45.2rem] mr-[3.2rem]" key={index}>
                  <ItemMobile itemProduct={item} />
                </div>
              ))
            ) : (
              <div className="w-full flex justify-center">
                <Image
                  src="/img/no-data.svg"
                  alt="banner-aboutus"
                  height={300}
                  width={300}
                />
              </div>
            )}
          </div>
        </div>
        {/* newest product */}
        <div>
          <div className="text-[2rem] not-italic font-extrabold text-[#313131] leading-[2.4rem] h-[2.4rem] mb-[2rem] max-md:text-[4.7rem] max-md:mb-[4.27rem]">
            SẢN PHẨM MỚI NHẤT
          </div>
          <div className="flex w-full max-md:flex-col">
            <Image
              width={400}
              height={300}
              // width={}
              className="object-cover w-2/5 h-[21.875rem] rounded-3xl mr-[1.25rem] max-md:w-full max-md:h-[60rem] max-md:rounded-[4.5rem] max-md:mb-[3rem]"
              src={dataAcf?.slideimage?.url}
              alt=""
            />
            <div className="grow w-full overflow-hidden h-[21.875rem] max-md:hidden">
              {dataListProductInit && dataListProductInit?.data?.length > 0 ? (
                <SlideProductComponent
                  keySlide="list-new-product"
                  breakPoint={{
                    PerView1280: 3,
                  }}
                  heightImage={17}
                  data={dataListProductInit?.data}
                  left
                />
              ) : (
                <div className="w-full flex justify-center">
                  <Image
                    src="/img/no-data.svg"
                    alt="banner-aboutus"
                    height={300}
                    width={300}
                  />
                </div>
              )}
            </div>
            <div className="hidden max-md:flex grow w-full overflow-x-auto hide-scrollbar-global overflow-hidden h-[66.26667rem]">
              {dataListProductInit && dataListProductInit?.data.length > 0 ? (
                map(dataListProductInit?.data, (item, index) => (
                  <div className="min-w-[45.2rem] mr-[3.2rem]" key={index}>
                    <ItemMobile itemProduct={item} />
                  </div>
                ))
              ) : (
                <div className="w-full flex justify-center">
                  <Image
                    src="/img/no-data.svg"
                    alt="banner-aboutus"
                    height={300}
                    width={300}
                  />
                </div>
              )}
            </div>
          </div>
        </div>
      </div>

      {/* outstanding product */}
    </div>
  );
}
