'use client';

import { Checkbox } from '@/components/ui/checkbox';
import './style.css';
import ItemProduct from '@/components/component-ui-custom/item-product/ItemProduct';
import ItemMobile from '@/components/component-ui-custom/item-product-mobile';
import { IDataPagination, IItemAttributeProduct } from '@/types/types-general';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import useSWR from 'swr';
import { postData } from '@/lib/post-data';

import Image from 'next/image';
import map from 'lodash.map';
import { useQueryStates } from 'next-usequerystate';
import ICFilter from '@/components/Icons/ICFilter';
import ICClear from '@/components/Icons/ICClear';
import { useBoolean } from '@/hooks/use-boolean';
import { cn } from '@/lib/utils';
import PaginationGlobal from '@/components/component-ui-custom/pagination-global';
import SortBy from '@/components/component-ui-custom/sortby';
import SkeletonItemProduct from '../skeleton-item-product';

interface IProps {
  // listAttribute: IItemAttributeProduct[];
  listAttribute: any;
  searchParams?: any;
  listAttributeNew?: any;
  routerSearchCustom: string;
}

interface IListAttributeConvert {
  label: string;
  listAttribute: any;
}

interface IParamsFilter {
  attribute: string;
  subAttribute: string[];
}
export default function FilterListProductChooseGlass(props: IProps) {
  const { listAttribute, searchParams, listAttributeNew, routerSearchCustom } =
    props;
  const refListProduct = useRef<any>();
  const isShowModalMobile = useBoolean(false);
  const [dataInit, setDataInit] = useState<any>([]);
  const [paramsFilter, setParamsFilter] = useState<IParamsFilter[]>([]);
  const [paramRouterGetApi, setParamRouterGetApi] = useState<any>('');

  const [isLoading, setIsLoading] = useState(true);

  const [sort, setSort] = useState('');

  const [dataPagination, setDataPagination] = useState<IDataPagination>({
    total: 12,
    currentPage: 1,
    perpage: 12,
  });

  const listAttributeConvert: IListAttributeConvert[] = [
    {
      label: 'Màu sắc',
      listAttribute: listAttribute?.color,
    },
    {
      label: 'Thương hiệu',
      listAttribute: listAttributeNew[0],
    },
    {
      label: 'Chất liệu',
      listAttribute: listAttributeNew[1],
    },
    {
      label: 'Hình dáng',
      listAttribute: listAttributeNew[2],
    },
    {
      label: 'Tính năng',
      listAttribute: listAttributeNew[3],
    },
  ];

  const bodyGetListProduct: any = useMemo(
    () => ({
      url: `wp-json/product/v1/products/filter?per_page=${
        dataPagination.perpage
      }&page=${dataPagination.currentPage}&${paramRouterGetApi}${
        sort !== '' ? `&price_order=${sort}` : ''
      }`,
      method: 'get',
    }),
    [
      dataPagination.currentPage,
      dataPagination.perpage,
      paramRouterGetApi,
      sort,
    ]
  );

  const fetcher = useCallback(async () => {
    setIsLoading(true);
    try {
      const res = await postData(bodyGetListProduct); // Assuming postData returns a promise
      setDataInit(res?.data);
      setDataPagination((prev) => ({
        ...prev,
        total: res?.countItem || 0,
      }));
    } finally {
      setIsLoading(false);
    }
  }, [bodyGetListProduct]);

  const getlistProduct = useSWR(bodyGetListProduct.url, fetcher, {
    revalidateOnFocus: false,
    revalidateIfStale: false,
    revalidateOnReconnect: false,
  });

  const handleScrollProduct = (): void => {
    const topPosition = refListProduct.current?.offsetTop - 200;
    window.scrollTo({
      top: topPosition,
      behavior: 'smooth',
    });
  };

  const onChange = (attribute?: string, slugSubAttribute?: string): void => {
    setParamsFilter((currentFilters: any) => {
      const index = currentFilters.findIndex(
        (item: any) => item.attribute === attribute
      );
      if (index === -1) {
        // Add new attribute with its sub-attribute
        return [
          ...currentFilters,
          {
            attribute: attribute ?? '',
            subAttribute: [slugSubAttribute ?? ''],
          },
        ];
      } else {
        // Update existing attribute by adding or removing sub-attribute
        return currentFilters.map((filter: any, idx: any) => {
          if (idx === index) {
            const isSubAttributePresent = filter.subAttribute.includes(
              slugSubAttribute ?? ''
            );
            return {
              ...filter,
              subAttribute: isSubAttributePresent
                ? filter.subAttribute.filter(
                    (sub: any) => sub !== slugSubAttribute
                  )
                : [...filter.subAttribute, slugSubAttribute ?? ''],
            };
          }
          return filter;
        });
      }
    });
  };

  useEffect(() => {
    if (paramsFilter?.length > 0) {
      let paramTmp = '';
      paramsFilter?.map((itemParams) => {
        if (itemParams?.subAttribute?.length > 0) {
          paramTmp += `${itemParams.attribute}=${itemParams?.subAttribute}&`;
        }
      });
      setParamRouterGetApi(paramTmp);
    }
  }, [paramsFilter]);

  useEffect(() => {
    handleScrollProduct();
  }, [dataPagination]);

  const handleSort = (val: any) => {
    if (val === 'none') {
      setSort('');
    } else {
      setSort(val);
    }
  };

  return (
    <div className="filter-list-product-container" ref={refListProduct}>
      {/* modal filter product mobile */}
      <div
        role="button"
        onClick={() => isShowModalMobile.onTrue()}
        className="hidden max-md:flex fixed bottom-[32rem] right-[4rem] z-20 h-[10.6rem] w-[10.6rem] rounded-full bg-[#55D5D2] justify-center items-center"
      >
        <ICFilter stroke="white" width="5.33333rem" height="5.33333rem" />
      </div>
      <div
        className={cn(
          'hidden max-md:block fixed bottom-0 bg-white left-0 z-50 w-full rounded-t-[3.2rem] shadow-modal-mobile transition-all duration-700',
          isShowModalMobile.value ? 'h-[90rem] pt-[3.2rem] pb-[5rem]' : 'h-0'
        )}
      >
        <div
          role="button"
          onClick={() => isShowModalMobile.onFalse()}
          className="flex justify-end padding-horizontal-mobile"
        >
          <ICClear />
        </div>
        <div className="overflow-y-auto w-full h-full px-[3.2rem]">
          {listAttributeConvert.map((item, index) => (
            <div key={index} className="w-full mb-[4rem]">
              <h3 className="text-[4.7rem] text-[#454545] leading-[4.8rem] not-italic font-bold mb-[2rem]">
                {item.label}
              </h3>
              <div className="grid grid-cols-2 gap-[2rem]">
                {item.listAttribute &&
                  item.listAttribute.map(
                    (itemSubAttribute: any, index: number) => (
                      <div
                        key={index}
                        className="flex items-center w-full overflow-hidden mb-[0.5rem]"
                      >
                        <Checkbox
                          onCheckedChange={(value: boolean) =>
                            onChange(
                              itemSubAttribute.taxonomy,
                              itemSubAttribute.slug
                            )
                          }
                          id={`${item.label}-${index}`}
                          className="border-[#ccc] border-[1px] h-[4rem] w-[4rem]"
                        >
                          <span className="font-medium line-clamp-1 text-nowrap ml-[0.7rem] text-[#454545] text-[4rem] leading-[5.5rem]">
                            {itemSubAttribute.name}
                          </span>
                        </Checkbox>
                      </div>
                    )
                  )}
              </div>
            </div>
          ))}
        </div>
      </div>

      <div className="flex justify-between  max-md:hidden pt-[2rem]">
        <div className="min-w-[15rem] overflow-hidden pr-[1rem]">
          {listAttributeConvert.map((item, index) => (
            <div key={index} className="w-full mb-[2.5rem]">
              <h3 className="title-filter">{item.label}</h3>
              {item.listAttribute &&
                item.listAttribute.map(
                  (itemSubAttribute: any, index: number) => (
                    <div
                      key={index}
                      className="flex items-center w-full overflow-hidden mb-[0.5rem]"
                    >
                      <Checkbox
                        disabled={isLoading}
                        onCheckedChange={(value: boolean) =>
                          onChange(
                            itemSubAttribute.taxonomy,
                            itemSubAttribute.slug
                          )
                        }
                        id={`${item.label}-${index}`}
                        className="border-[#ccc] border-[1px]"
                      >
                        <span className="font-medium line-clamp-1 text-nowrap ml-[0.7rem]">
                          {itemSubAttribute.name}
                        </span>
                      </Checkbox>
                    </div>
                  )
                )}
            </div>
          ))}
        </div>
        <div className="grow">
          <div className="ml-auto w-fit mb-[1rem] max-md:mb-[2rem]">
            <SortBy handleSort={handleSort} />
          </div>
          {dataInit && dataInit?.length === 0 && (
            <div className="flex justify-center">
              <Image
                src="/img/no-data.svg"
                alt="banner-aboutus"
                height={300}
                width={300}
              />
            </div>
          )}
          {dataInit && dataInit?.length > 0 && isLoading ? (
            <SkeletonItemProduct />
          ) : (
            <div className="grid grid-cols-4 gap-4">
              {dataInit &&
                dataInit.length > 0 &&
                dataInit?.map((item: any, index: number) => (
                  <div className="rounded-[1rem]" key={index}>
                    <ItemProduct item={item} />
                  </div>
                ))}
            </div>
          )}

          <div className="flex justify-end mt-[2rem]">
            <PaginationGlobal
              dataPagination={dataPagination}
              setDataPagination={setDataPagination}
            />
          </div>
        </div>
      </div>

      <div className="hidden max-md:block">
        <div className="ml-auto w-fit mb-[1rem] max-md:mb-[2rem]">
          <SortBy handleSort={handleSort} />
        </div>
        {dataInit && dataInit?.length === 0 && (
          <div className="flex justify-center">
            <Image
              src="/img/no-data.svg"
              alt="banner-aboutus"
              height={300}
              width={300}
            />
          </div>
        )}
        {dataInit && dataInit?.length > 0 && isLoading ? (
          <SkeletonItemProduct />
        ) : (
          <div className="grid grid-cols-2">
            {map(dataInit, (item: any, index: number) => (
              <div key={index} className="relative mb-[4.27rem]">
                <ItemMobile itemProduct={item} />
              </div>
            ))}
          </div>
        )}

        <div className="flex justify-end mt-[2rem]">
          <PaginationGlobal
            dataPagination={dataPagination}
            setDataPagination={setDataPagination}
          />
        </div>
      </div>
    </div>
  );
}
