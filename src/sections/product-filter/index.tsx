'use client';

import './style.css';
import FilterListProductChooseGlass from './components/filter-list-product-choose-glasses';
import Image from 'next/image';
import { IItemAttributeProduct } from '@/types/types-general';
import Link from 'next/link';
import ProductListFilter from '../ProductListFilter';
import { useCallback, useMemo, useState } from 'react';
import { postData } from '@/lib/post-data';
import useSWR from 'swr';

interface IProps {
  routerSearchCustom: string;
  dataListAttribute: any;
  searchParams?: any;
  listAttributeNew?: any;
  dataAcf?: any;
}

export default function ProductFilter(props: IProps) {
  const {
    routerSearchCustom,
    dataListAttribute,
    searchParams,
    listAttributeNew,
    dataAcf,
  } = props;

  const [dataInit, setDataInit] = useState({});

  const bodyGetListProduct: any = useMemo(() => {
    return {
      url: `wp-json/product/v1/products/filter?per_page=${24}&page=${1}&${routerSearchCustom}`,
      method: 'get',
    };
  }, [routerSearchCustom]);

  const fetcher = useCallback(async () => {
    const res = await postData(bodyGetListProduct); // Assuming postData returns a promise

    setDataInit(res);
  }, [bodyGetListProduct]);

  const getlistProduct = useSWR(
    bodyGetListProduct.url,
    bodyGetListProduct.url ? fetcher : null,
    {
      revalidateOnFocus: false,
      revalidateIfStale: false,
      revalidateOnReconnect: false,
    }
  );

  return (
    <div className="list-product-container mb-[2.94rem]">
      {/* banner */}
      <div className="height-banner-global relative bg-banner-about-us bg-cover bg-no-repeat w-full">
        <Image
          src={dataAcf?.image?.url || '/img/about-us/bg-banner-about-us.jpg'}
          width={1920}
          height={1080}
          alt="banner cart"
          className="w-full h-full object-cover"
          priority
        />
        <div className="absolute bottom-20 left-[8rem]">
          {/* <h1 className="text-white text-[3.125rem] leading-[4.6875rem] font-bold not-italic max-md:hidden max-md:font-bold max-md:text-[4.8rem] max-md:leading-[7.2rem]">
            LỜI CẢM ƠN
          </h1> */}
          <div className="flex items-center">
            <Link
              href={'/'}
              className="text-white text-[0.875rem] font-semibold leading-[2.25rem] not-italic max-md:text-[3.2rem]"
            >
              Trang chủ
            </Link>
            <div className="bg-[#81C8C2] h-[0.625rem] w-[0.625rem] rounded-full mx-[1rem] max-md:w-[2.13333rem] max-md:h-[2.13333rem] max-md:mx-[2rem]" />
            <span className="text-white text-[0.875rem] font-semibold leading-[2.25rem] not-italic max-md:text-[3.2rem] max-md:leading-[4.8rem]">
              Cửa hàng
            </span>
          </div>
        </div>
      </div>

      {/* content */}
      <div className="w-[87.5rem] mx-auto mt-[2.5rem] max-md:w-full max-md:px-[3.2rem] max-md:mt-[3.2rem]">
        {/* <FilterListProductChooseGlass
          routerSearchCustom={routerSearchCustom}
          listAttribute={dataListAttribute}
          searchParams={searchParams}
          listAttributeNew={listAttributeNew}
        /> */}

        <ProductListFilter
          listAttribute={dataListAttribute}
          searchParams={searchParams}
          listAttributeNew={listAttributeNew}
          routerSearchCustom={routerSearchCustom}
          type="filter"
          dataListInit={dataInit}
        />
      </div>
    </div>
  );
}
