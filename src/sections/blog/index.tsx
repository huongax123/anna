'use client';

import BannerBlog from '@/sections/blog/Banner';
import ServiceBlog from '@/sections/blog/Service';
import SectionHome from '@/sections/home/view/SectionHome';
import './style.css';
import ListBlog from '@/sections/blog/ListBlog';
import { baseUrl, fetchDataRest } from '@/lib/fetch-data-rest';
import useSWR from 'swr';
import { useEffect, useRef, useState } from 'react';
import SkeletonItemProduct from '../ProductListFilter/skeleton-item-product';

interface IProps {
  listPostRes?: any;
  totalPostRes?: any;
  dataInit?: any;
  dataAcf?: any;
  dataSectionHome?: any;
}
function Blog({ dataInit, dataAcf, dataSectionHome }: IProps) {
  const [listBlog, setListBlog] = useState<any>([]);
  const firstLoad = useRef(true);
  const [dataPagination, setDataPagination] = useState({
    total: 0,
    currentPage: 1,
    perpage: 9,
  });

  useEffect(() => {
    firstLoad.current = false;
    if (dataInit?.item) setListBlog(dataInit?.item || []);
    if (dataInit?.countItem)
      setDataPagination((prev) => ({
        ...prev,
        total: dataInit?.countItem || 0,
      }));
  }, [dataInit]);

  const getListProduct = useSWR(
    `${baseUrl}post/v1/posts?per_page=9&page=${dataPagination?.currentPage}`,
    !firstLoad.current
      ? () =>
        fetchDataRest(
          'GET',
          `post/v1/posts?per_page=9&page=${dataPagination?.currentPage}`
        ).then((res: any) => {
          setListBlog(res?.item);
        })
      : null,
    {
      revalidateOnFocus: false,
      revalidateIfStale: false,
      revalidateOnReconnect: false,
    }
  );

  return (
    <div>
      <BannerBlog dataAcf={dataAcf} />
      <div className="relative">
        {getListProduct.isLoading ? (
          <div className="container">
            <SkeletonItemProduct />
          </div>
        ) : (
          <div className="container-custom">
            <ServiceBlog
              blogItemFirst={listBlog && listBlog.length > 0 ? listBlog[0] : []}
            />
            <ListBlog
              listBlog={listBlog}
              dataPagination={dataPagination}
              setDataPagination={setDataPagination}
            />
          </div>
        )}
      </div>

      <SectionHome data={dataSectionHome} />
    </div>
  );
}

export default Blog;
