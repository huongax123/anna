import Statistics from '@/sections/httt/components/statistics';
import Action from './components/action';
import Banner from './components/banner';
import Video from './components/video';
import SectionHome from '@/sections/home/view/SectionHome';
import Share from '@/sections/httt/components/share';
import ThanksForJoin from '@/sections/httt/components/thanksforjoin';
import About from '@/sections/httt/components/about';
import Story from '@/sections/httt/components/story';
import Partner from '@/sections/httt/components/partner';
import LovingConnect from '@/sections/httt/components/lovingconnect';
import SlideTmp from '@/sections/httt/components/SlideTmp';
interface IProps {
  data?: any;
  banner?: any;
}
export default function HTTT(props: IProps) {
  return (
    <>
      <Banner data={props?.data} />
      <Video data={props?.data} />
      <Action data={props?.data} />
      <Statistics data={props?.data} />
      {/* <SlideTmp /> */}
      <Share data={props?.data} />
      {/* <ThanksForJoin /> */}
      <About data={props?.data} />
      <Story data={props?.data} />
      <Partner data={props?.data} />
      <LovingConnect data={props?.data} />
      <SectionHome data={props?.data} />
    </>
  );
}
