import React from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/pagination';

// import './styles.css';
import { Autoplay } from 'swiper/modules';
import ActionItemSlide from '@/sections/httt/components/action/popup/slide/Item';

interface IProps {
  isReverse: boolean;
  data:any;
}
function SlideAction({ isReverse,data }: IProps) {
  return (
    <Swiper
      autoplay={{
        delay: 0,
        disableOnInteraction: false,
        reverseDirection: isReverse,
      }}
      speed={5000}
      loop
      direction="vertical"
      breakpoints={{
        0: {
          slidesPerView: 3,
        },
        1024: {
          slidesPerView: 2,
        },
      }}
      pagination={{
        clickable: true,
      }}
      modules={[Autoplay]}
      className="mySwiperAction max-h-[187rem] md:max-h-[54.87rem]"
    >
      {data?.map((item: any, index: number) => (
        <SwiperSlide key={index}>
          <ActionItemSlide img={item?.image?.url} des={item?.desc} name={item?.name} address={item.location} />
        </SwiperSlide>
      ))}


    </Swiper>
  );
}

export default SlideAction;
