'use client';

import { cn } from '@/lib/utils';
import Image from 'next/image';
import { useRef, useState } from 'react';
interface IProps {
  data?: any;
}
export default function Video(props: IProps) {
  const data = props?.data?.video || {};
  const vidRef = useRef(null) as any;
  const [isControl, setIsControl] = useState(false);
  const handlePlayVideo = () => {
    setIsControl(!isControl);
    if (isControl === true) {
      vidRef.current.pause();
    } else {
      vidRef.current.play();
    }
  };
  return (
    <div className="w-full h-[75.46667rem] md:h-[47.125rem] mt-[0rem] max-md:mt-0 relative">
      <video
        src={data?.video?.link || '/img/httt/video.mp4'}
        className="w-full h-full object-cover"
        ref={vidRef}
        poster="/img/httt/poster.jpg"
        controls={isControl}
      />
      <Image
        src="/img/httt/play.svg"
        alt="poster"
        className={cn(
          'w-[7rem] h-[7rem] absolute top-1/2 -translate-x-1/2 left-1/2 -translate-y-1/2 z-10 object-cover cursor-pointer',
          isControl ? 'hidden' : 'block'
        )}
        height={120}
        width={120}
        onClick={handlePlayVideo}
      />
    </div>
  );
}
