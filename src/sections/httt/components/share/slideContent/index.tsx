'use client';

import React from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/navigation';
import { Controller, Navigation } from 'swiper/modules';
import ContentItem from '@/sections/httt/components/share/slideContent/Item';

function SlideContentShare({ setControlledSwiper, data }: any) {
  return (
    <div className="relative">
      <Swiper
        breakpoints={{
          0: {
            slidesPerView: 1,
          },
          767: {
            slidesPerView: 1,
          },
          1024: {
            slidesPerView: 1,
          },
        }}
        loop
        speed={600}
        navigation={{
          prevEl: `.prev-11111-tmp`,
          nextEl: `.next-11111-tmp`,
        }}
        modules={[Navigation, Controller]}
        onSwiper={setControlledSwiper}
        className="myShareContent pl-[0rem] mx-[1rem]"
      >
        {data?.map((item: any, index: number) => (
          <SwiperSlide className="swiper-no-swiping" key={index}>
            <ContentItem
              title1={item?.name}
              info={item?.info}
              title2={item?.title}
              des={item?.desc}
            />
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  );
}

export default SlideContentShare;
