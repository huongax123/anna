import SectionHome from '@/sections/home/view/SectionHome';
import './style.css';

interface IPropPolicy {
  dataPolicyRender?: any;
  dataSectionHome?: any;
}

function Policy({ dataPolicyRender, dataSectionHome }: IPropPolicy) {
  return (
    <div className="mt-[6rem]">
      <div className="container-custom py-24 md:py-12 md:px-4 m-auto max-sm:pt-[12rem]">
        <h1 className="text-black text-[7.5rem] md:text-[3.5rem] font-semibold max-md:pb-6 pb-10">
          {dataPolicyRender?.title?.rendered}
        </h1>
        <div className="flex justify-between flex-wrap kKao4-container">
          <div
            className="w-full min-h-[300px] policy-page font-semibold min-w-full []"
            dangerouslySetInnerHTML={{
              __html: `${
                dataPolicyRender ? dataPolicyRender?.content?.rendered : ''
              }`,
            }}
          />
        </div>
      </div>
      <SectionHome data={dataSectionHome} />
    </div>
  );
}

export default Policy;
