'use client';

import 'swiper/css';
import ItemSocial from '@/sections/home/view/Social/List/Item';
import ICYoutube from '@/components/Icons/ICYoutube';
import ICInstagramFooter from '@/components/Icons/ICInstagramFooter';
import ItemV2 from './ItemV2';
import { useContext, useRef } from 'react';
import { Autoplay, FreeMode } from 'swiper/modules';
import { Swiper, SwiperSlide } from 'swiper/react';
import ICTikTok from '@/components/Icons/ICTikTok';
import ICFaceSocial from '@/components/Icons/ICFaceSocial';
import { ProductCartContext } from '@/context-provider';

function ListSocial({ dataSocial }: any) {
  const boxRef = useRef<any>(null);
  const { linkSocial } = useContext(ProductCartContext);

  return (
    <div className="relative overflow-hidden h-[31.9rem]">
      <div ref={boxRef} className="w-full h-full">
        <Swiper
          slidesPerView="auto"
          centeredSlides
          autoplay={{
            delay: 0,
            disableOnInteraction: false,
            pauseOnMouseEnter: true,
          }}
          speed={5000}
          spaceBetween={0}
          loop
          modules={[Autoplay, FreeMode]}
          id="swiper_social"
          className="w-full h-full"
        >
          <SwiperSlide className="h-full !w-[47.6rem] !flex">
            <div className="text-[#CAF2F1] text-[8rem] md:text-[4.25rem] font-black w-[23.8rem] h-[31.9rem]">
              <ItemSocial
                linkSocial={linkSocial?.facebook || '/'}
                img={dataSocial?.facebook?.image}
                icon={<ICFaceSocial width={53} height={100} />}
                social="Facebook"
                infor={dataSocial?.facebook?.name || ''}
              />
            </div>
            <div className="text-[#CAF2F1] text-[8rem] md:text-[4.25rem] font-black w-[23.8rem] h-[31.9rem]">
              <ItemV2
                title="HÀNH TRÌNH TỬ TẾ"
                description="Chúng mình là kính mắt Anna kính mắt của sự tử tế"
              />
            </div>
          </SwiperSlide>
          <SwiperSlide className="h-full !w-[47.6rem] !flex">
            <div className="text-[#CAF2F1] text-[8rem] md:text-[4.25rem] font-black w-[23.8rem] h-[31.9rem]">
              <ItemSocial
                linkSocial={linkSocial?.instagram || '/'}
                img={dataSocial?.instagram?.image}
                icon={<ICInstagramFooter width={100} height={100} />}
                social="Instagram"
                infor={dataSocial?.instagram?.name || ''}
              />
            </div>
            <div className="text-[#CAF2F1] text-[8rem] md:text-[4.25rem] font-black w-[23.8rem] h-[31.9rem]">
              <ItemV2
                title="HÀNH TRÌNH TỬ TẾ"
                description="Chúng mình là kính mắt Anna kính mắt của sự tử tế"
              />
            </div>
          </SwiperSlide>
          <SwiperSlide className="h-full !w-[47.6rem] !flex">
            <div className="text-[#CAF2F1] text-[8rem] md:text-[4.25rem] font-black w-[23.8rem] h-[31.9rem]">
              <ItemSocial
                linkSocial={linkSocial?.youtube || '/'}
                img={dataSocial?.youtube?.image}
                icon={<ICYoutube fill="white" />}
                social="Youtube"
                infor={dataSocial?.youtube?.name || ''}
              />
            </div>
            <div className="text-[#CAF2F1] text-[8rem] md:text-[4.25rem] font-black w-[23.8rem] h-[31.9rem]">
              <ItemV2
                title="HÀNH TRÌNH TỬ TẾ"
                description="Chúng mình là kính mắt Anna kính mắt của sự tử tế"
              />
            </div>
          </SwiperSlide>
          <SwiperSlide className="h-full !w-[47.6rem] !flex">
            <div className="text-[#CAF2F1] text-[8rem] md:text-[4.25rem] font-black w-[23.8rem] h-[31.9rem]">
              <ItemSocial
                linkSocial={linkSocial?.tiktok || '/'}
                img={dataSocial?.tiktok?.image}
                icon={<ICTikTok width={82} height={104} />}
                social="TikTok"
                infor={dataSocial?.tiktok?.name || ''}
              />
            </div>
            <div className="text-[#CAF2F1] text-[8rem] md:text-[4.25rem] font-black w-[23.8rem] h-[31.9rem]">
              <ItemV2
                title="HÀNH TRÌNH TỬ TẾ"
                description="Chúng mình là kính mắt Anna kính mắt của sự tử tế"
              />
            </div>
          </SwiperSlide>
          <SwiperSlide className="h-full !w-[47.6rem] !flex">
            <div className="text-[#CAF2F1] text-[8rem] md:text-[4.25rem] font-black w-[23.8rem] h-[31.9rem]">
              <ItemSocial
                linkSocial={linkSocial?.instagram || '/'}
                img={dataSocial?.instagram?.image}
                icon={<ICInstagramFooter width={100} height={100} />}
                social="Instagram"
                infor={dataSocial?.instagram?.name || ''}
              />
            </div>
            <div className="text-[#CAF2F1] text-[8rem] md:text-[4.25rem] font-black w-[23.8rem] h-[31.9rem]">
              <ItemV2
                title="HÀNH TRÌNH TỬ TẾ"
                description="Chúng mình là kính mắt Anna kính mắt của sự tử tế"
              />
            </div>
          </SwiperSlide>
          <SwiperSlide className="h-full !w-[47.6rem] !flex">
            <div className="text-[#CAF2F1] text-[8rem] md:text-[4.25rem] font-black w-[23.8rem] h-[31.9rem]">
              <ItemSocial
                linkSocial={linkSocial?.youtube || '/'}
                img={dataSocial?.youtube?.image}
                icon={<ICYoutube fill="white" />}
                social="Youtube"
                infor={dataSocial?.youtube?.name || ''}
              />
            </div>
            <div className="text-[#CAF2F1] text-[8rem] md:text-[4.25rem] font-black w-[23.8rem] h-[31.9rem]">
              <ItemV2
                title="HÀNH TRÌNH TỬ TẾ"
                description="Chúng mình là kính mắt Anna kính mắt của sự tử tế"
              />
            </div>
          </SwiperSlide>
        </Swiper>
      </div>
    </div>
  );
}

export default ListSocial;
