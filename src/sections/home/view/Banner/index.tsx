'use client';

// Import Swiper React components
import { Swiper, SwiperSlide } from 'swiper/react';

// Import Swiper styles
import Image from 'next/image';
import 'swiper/css';
import { Pagination } from 'swiper/modules';
import React from 'react';
import Link from 'next/link';

interface IpropBanner {
  dataBanner: any;
  dataBannerMb: any;
}
function BannerHome({ dataBanner, dataBannerMb }: IpropBanner) {
  return (
    <div>
      <div className="hidden sm:block">
        <Swiper watchSlidesProgress slidesPerView={1} loop className="sm:h-fit">
          {dataBanner?.map((valueBanner: any, index: number) => (
            <SwiperSlide key={index}>
              <Link
                href={valueBanner?.link}
                className=" max-md:h-[65rem] max-lg:h-[50rem] h-screen w-full flex"
              >
                <Image
                  src={valueBanner?.image?.url}
                  alt={valueBanner?.image?.alt}
                  fill
                  priority
                  className="w-full object-cover  max-md:h-[65rem] max-lg:h-[50rem] h-screen "
                />
              </Link>
            </SwiperSlide>
          ))}
        </Swiper>
      </div>
      <div className="block sm:hidden">
        <Swiper
          watchSlidesProgress
          slidesPerView={1}
          loop
          pagination={{
            clickable: true,
          }}
          modules={[Pagination]}
          className="sm:h-fit banner-pagination"
        >
          {dataBannerMb?.map((valueBanner: any, index: number) => (
            <SwiperSlide key={index}>
              <Link
                href={valueBanner?.link}
                className="w-full h-[93.06667rem] md:h-[53rem] flex"
              >
                <Image
                  src={valueBanner?.image?.url}
                  alt={valueBanner?.image?.alt}
                  quality={80}
                  fill
                  priority
                  className="w-full object-cover h-[93.06667rem] md:h-[53rem]"
                />
              </Link>
            </SwiperSlide>
          ))}
        </Swiper>
      </div>
    </div>
  );
}

export default BannerHome;
