import SectionHome from '@/sections/home/view/SectionHome';
import NewCollection from '@/sections/home/view/CollectNew';
import Feature from '@/sections/home/view/Feature';
import FlashSale from '@/sections/home/view/FlashSale';
import './style.css';
// import AboutHome from '@/sections/home/view/About';
import Social from '@/sections/home/view/Social';
import ListSocial from '@/sections/home/view/Social/List';
import Blog from '@/sections/home/view/Blog';
import ActionHome from '@/sections/home/view/Action';
import { fetchDataRest } from '@/lib/fetch-data-rest';
import BannerHome from '@/sections/home/view/Banner';
import SlideMobileSocial from './Social/SlideMobile';
import AboutHome from '@/sections/home/view/About';
import React from 'react';
import MascotHome from '@/sections/home/view/Mascot';
import PopupGeneral from '@/components/component-ui-custom/popup-general';

const Home = async () => {
  const dataHomeFetch = fetchDataRest('GET', 'acf/v3/posts/334');
  const dataProductSaleFetch = fetchDataRest('GET', 'custom/v1/product-sales');
  const dataSellingProductFetch = fetchDataRest(
    'GET',
    'custom/v1/best-selling-products?order=asc&limit=12'
  );
  const dataProductGKFetch = fetchDataRest(
    'GET',
    'custom/v1/products-by-category?category=gong-kinh&per_page=8&page=1'
  );
  const dataProductTKFetch = fetchDataRest(
    'GET',
    'custom/v1/products-by-category?category=trong-kinh&per_page=8&page=1'
  );
  const dataProductKRFetch = fetchDataRest(
    'GET',
    'custom/v1/products-by-category?category=kinh-ram&per_page=8&page=1'
  );
  const dataProductKATFetch = fetchDataRest(
    'GET',
    'custom/v1/products-by-category?category=kinh-ap-trong&per_page=8&page=1'
  );
  const dataAcf = fetchDataRest('GET', 'acf/v3/posts/3014/last_section');
  const [
    dataHome,
    dataProductSale,
    dataSellingProduct,
    dataProductGK,
    dataProductTK,
    dataProductKAT,
    dataProductKR,
    dataAcfItem,
  ] = await Promise.all([
    dataHomeFetch,
    dataProductSaleFetch,
    dataSellingProductFetch,
    dataProductGKFetch,
    dataProductTKFetch,
    dataProductKATFetch,
    dataProductKRFetch,
    dataAcf,
  ]);
  // const dataProductKR = { data: [] };
  return (
    <div className="min-h-full flex flex-col">
      <h1 className="hidden">Kính Mắt Anna</h1>
      <MascotHome isShow={dataHome?.acf?.is_tuna} />

      <div className="banner">
        <BannerHome
          dataBanner={dataHome?.acf?.banner}
          dataBannerMb={dataHome?.acf?.banner_mobile}
        />
      </div>
      <FlashSale
        smallBanner1={dataHome?.acf?.small_banner_1}
        smallBanner2={dataHome?.acf?.small_banner_2}
        dataProductSale={dataProductSale}
        dataSellingProduct={dataSellingProduct}
      />
      <NewCollection
        dataProductGK={dataProductGK}
        dataProductTK={dataProductTK}
        dataProductKR={dataProductKR || []}
        dataProductKAT={dataProductKAT}
      />
      <div className="hidden md:block">
        <SectionHome data={dataAcfItem} />
      </div>
      <Feature dataFeature={dataHome?.acf?.for_you[0]} />
      <AboutHome dataAbout={dataHome?.acf?.about[0]} />
      <Social />
      <SlideMobileSocial dataSocial={dataHome?.acf?.anna_on_social} />
      <div className="hidden md:block">
        <ListSocial dataSocial={dataHome?.acf?.anna_on_social} />
      </div>
      <div className="max-sm:bg-[#F8F8F8] md:py-[5.75rem]">
        <Blog />
      </div>
      <ActionHome dataTrip={dataHome?.acf?.trip[0]} />

      {dataHome?.acf?.popup?.img?.url && (
        <PopupGeneral dataPopup={dataHome?.acf?.popup} />
      )}
    </div>
  );
};

export default Home;
