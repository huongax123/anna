import SlideProductComponent from '@/components/component-ui-custom/slide-swiper-product/slide-product';
import Image from 'next/image';
import React from 'react';

interface IProps {
  relatedData: any;
}

function RecommendProduct(props: IProps) {
  const { relatedData } = props;

  return (
    <div className="bg-[#EEFBFB]">
      <div className="w-[87.5rem] mx-auto max-lg:mx-[3.25rem] max-md:mx-0 pb-[6.38rem] max-md:w-full max-md:pl-[2.67rem] max-md:bg-[#EEFBFB] max-md:pb-[9.6rem]">
        <h2 className="pt-[2.06rem] text-[2rem] font-extrabold text-[#4DC0BD] max-md:text-[4.8rem] max-md:pt-[6.4rem] max-md:text-[#313131] text-left">
          GỢI Ý CHO BẠN
        </h2>
        <div className="flex w-full justify-between mt-[2.12rem] overflow-hidden overflow-x-auto hide-scrollbar-global max-md:mt-[4.27rem]">
          <SlideProductComponent keySlide="flash-sale" data={relatedData} />
        </div>
      </div>
    </div>
  );
}

export default RecommendProduct;
