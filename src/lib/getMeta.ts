export const getMeta = (result:any, slug:any) =>{

  const baseURL = process.env.NEXT_PUBLIC_BASE_URL
    const meta={
        metadataBase: new URL(`${baseURL}`),
        title: result?.json?.title,
        description: result?.json?.description,
        alternates: {
            canonical: `${baseURL}/${slug}`,
          },
        openGraph: {
          title: result?.json?.title,
          description: result?.json?.description,
          url: `${baseURL}/${slug}`,
          siteName: result?.json?.og_site_name,
          images: [] as { url: string; width: number; height: number }[],
          locale: result?.json?.og_locale,
          type: "website",
        },
        twitter: {
            card: 'summary_large_image',
            title: result?.json?.title,
            description: result?.json?.description,
            creator: process.env.NEXT_PUBLIC_REST_API,
            images: [] as { url: string}[], 
        },
        verification: {
          google: '22oQMoX1lOLyZ6Qs2MGD3yb1JTOEfadEmmB2tAVdFh0',
        }

    }
    if (result?.json?.og_image && result.json.og_image.length > 0) {
        meta.openGraph.images.push({
          url: result.json.og_image[0]?.url,
          width: result.json.og_image[0]?.width,
          height: result.json.og_image[0]?.height
        });
        meta.twitter.images.push({
            url: result.json.og_image[0]?.url
        });
    }
    return meta
      
}