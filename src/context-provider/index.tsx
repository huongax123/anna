'use client';

import { createContext, useEffect, useState } from 'react';
import { IItemCart } from '@/types/types-general';
import { keyLocalStorage } from '@/configs/config';
import { SessionProvider } from 'next-auth/react';
import { usePathname } from 'next/navigation';
import { fetchDataAuthen } from '@/lib/post-data';
import { useBoolean } from '@/hooks/use-boolean';
import ChooseGlasses from '@/components/component-ui-custom/choose-glasses';
import { fetchDataRest } from '@/lib/fetch-data-rest';

export const ProductCartContext = createContext<any>({});
export function ContextProvider({
  children,
  session,
  listAttributeChooseGlasses,
}: any) {
  const pathname = usePathname();
  const isShowPopupChooseGlasses = useBoolean(false);

  const [inforUserGlobal, setInfoUserGlobal] = useState<any>(undefined);
  const [listCartGlobal, setListCartGlobal] = useState<IItemCart[]>([]);
  const [linkSocial, setLinkSocial] = useState<any>({});

  // CART CONTEXT
  const handleChangeDataCartGlobal = (data: IItemCart[]): void => {
    setListCartGlobal(data);
  };

  const clearDataCartProductContext = (): void => {
    setListCartGlobal([]);
    localStorage.setItem(keyLocalStorage.keyProductsInCart, '[]');
  };
  // END CONTEXT

  const isLogin = (): boolean => {
    return session !== null;
  };

  // GET API info user
  let bodyGetInfoUser: any;
  // GET API cart
  let bodyGetCart: any;
  // GET LIST Wishlist
  let bodyGetListWishList: any;

  if (session !== null && session?.user?.token && session?.user?.token !== '') {
    bodyGetInfoUser = {
      url: `wp-json/custom-woo-api/v1/customer`,
      method: 'get',
      token: session?.user?.token,
    };

    bodyGetCart = {
      url: `/wp-json/woocart/v1/cart`,
      method: 'get',
      token: session?.user?.token,
    };

    bodyGetListWishList = {
      url: `wp-json/custom/v1/get-wishlist`,
      method: 'get',
      token: session?.user?.token,
    };
  }

  // const bodyGetSocial = {
  //   url: `acf/v3/posts/334`,
  //   method: 'get',
  // };

  useEffect(() => {
    const getLinkSocial = async () => {
      const res = await fetchDataRest('GET', 'acf/v3/posts/334');
      setLinkSocial(res?.acf?.social);
    };
    getLinkSocial();
    if (
      session === null &&
      typeof window !== 'undefined' &&
      localStorage.getItem(keyLocalStorage.keyProductsInCart) !== null &&
      localStorage.getItem(keyLocalStorage.keyProductsInCart) !== undefined
    ) {
      const storedData = localStorage.getItem(
        keyLocalStorage.keyProductsInCart
      ) as string;

      setListCartGlobal(JSON.parse(storedData));
      return;
    }

    Promise.all([
      fetchDataAuthen(bodyGetInfoUser),
      fetchDataAuthen(bodyGetCart),
      fetchDataAuthen(bodyGetListWishList),
    ])
      .then((res) => {
        const [userInfo, listCart, listWishlist] = res;
        setInfoUserGlobal(userInfo);
        setListCartGlobal(listCart);

        if (listWishlist) {
          const listIdWishlist = listWishlist.map((item: any) =>
            parseInt(item.wishlist_id, 10)
          );

          localStorage.setItem(
            keyLocalStorage.keyProductWishlist,
            JSON.stringify(listIdWishlist)
          );
        }
      })
      .catch((error) => {
        console.log('error');
      });
  }, []);

  return (
    <SessionProvider>
      <ProductCartContext.Provider
        value={{
          handleChangeDataCartGlobal,
          clearDataCartProductContext,
          listCartGlobal,
          isLogin,
          inforUserGlobal,
          isShowPopupChooseGlasses,
          linkSocial,
        }}
      >
        <ChooseGlasses
          isShowPopupChooseGlasses={isShowPopupChooseGlasses}
          listAttributeChooseGlasses={listAttributeChooseGlasses}
        />
        {children}
      </ProductCartContext.Provider>
    </SessionProvider>
  );
}
