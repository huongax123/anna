import { postData } from '@/lib/post-data';
const GET_POSTS = {
  url: `wp-json/post/v1/posts?per_page=9&page=1`,
  method: 'get',
};

const GET_PRODUCTS = {
  url: `wp-json/custom/v1/products`,
  method: 'get',
};
const GET_CATEGORIES = {
  url: `wp-json/custom/v1/categories`,
  method: 'get',
};

export default async function sitemap() {
  // const posts = await postData(GET_POSTS);
  // const products = await postData(GET_PRODUCTS);
  // const categories = await postData(GET_CATEGORIES);

  const [posts, products, categories] = await Promise.all([
    postData(GET_POSTS),
    postData(GET_PRODUCTS),
    postData(GET_CATEGORIES),
  ]);

  const fixPages = [
    {
      url: process.env.NEXT_PUBLIC_BASE_URL,
      lastModified: new Date(),
      priority: 1,
    },
    {
      url: `${process.env.NEXT_PUBLIC_BASE_URL}/cua-hang`,
      lastModified: new Date(),
      priority: 0.9,
    },
    // Tìm CỬA HÀNG
    {
      url: `${process.env.NEXT_PUBLIC_BASE_URL}/he-thong-cua-hang`,
      lastModified: new Date(),
      priority: 0.9,
    },
    // XEM THÊM
    {
      url: `${process.env.NEXT_PUBLIC_BASE_URL}/ve-anna`,
      lastModified: new Date(),
      priority: 0.9,
    },
    {
      url: `${process.env.NEXT_PUBLIC_BASE_URL}/blog`,
      lastModified: new Date(),
      priority: 0.9,
    },
    {
      url: `${process.env.NEXT_PUBLIC_BASE_URL}/product-filter`,
      lastModified: new Date(),
      priority: 0.9,
    },
    {
      url: `${process.env.NEXT_PUBLIC_BASE_URL}/nguyen-tac-chon-mat-kinh`,
      lastModified: new Date(),
      priority: 0.9,
    },
    {
      url: `${process.env.NEXT_PUBLIC_BASE_URL}/khoang-cach-dong-tu-pd-chinh-xac-nhat`,
      lastModified: new Date(),
      priority: 0.9,
    },
    {
      url: `${process.env.NEXT_PUBLIC_BASE_URL}/huong-dan-chon-size-mat-kinh-vua-khuon-mat`,
      lastModified: new Date(),
      priority: 0.9,
    },
    // HÀNH TRÌNH TỬ TẾ
    {
      url: `${process.env.NEXT_PUBLIC_BASE_URL}/hanh-trinh-tu-te`,
      lastModified: new Date(),
      priority: 0.9,
    },
    // GIỎ HÀNG
    {
      url: `${process.env.NEXT_PUBLIC_BASE_URL}/gio-hang`,
      lastModified: new Date(),
      priority: 0.9,
    },
    // CHÍNH SÁCH BẢO HÀNH, CHÍNH SÁCH BẢO HÀNH, CHÍNH SÁCH BẢO HÀNH, CHÍNH SÁCH
    {
      url: `${process.env.NEXT_PUBLIC_BASE_URL}/chinh-sach-bao-hanh`,
      lastModified: new Date(),
      priority: 0.9,
    },
    {
      url: `${process.env.NEXT_PUBLIC_BASE_URL}/do-mat-mien-phi`,
      lastModified: new Date(),
      priority: 0.9,
    },
    {
      url: `${process.env.NEXT_PUBLIC_BASE_URL}/thu-cu-doi-moi`,
      lastModified: new Date(),
      priority: 0.9,
    },
    {
      url: `${process.env.NEXT_PUBLIC_BASE_URL}/ve-sinh-va-bao-quan-kinh`,
      lastModified: new Date(),
      priority: 0.9,
    },
    // TRA CỨU ĐƠN HÀNG TRA CỨU ĐƠN HÀNG TRA CỨU ĐƠN HÀNG
    {
      url: `${process.env.NEXT_PUBLIC_BASE_URL}/order-checking`,
      lastModified: new Date(),
      priority: 0.9,
    },
    // TÀI KHOẢN - ĐĂNG KÍ
    {
      url: `${process.env.NEXT_PUBLIC_BASE_URL}/tai-khoan`,
      lastModified: new Date(),
      priority: 0.9,
    },
    {
      url: `${process.env.NEXT_PUBLIC_BASE_URL}/dang-ky`,
      lastModified: new Date(),
      priority: 0.9,
    },
    // FOOTER
    {
      url: `${process.env.NEXT_PUBLIC_BASE_URL}/chinh-sach-thanh-toan`,
      lastModified: new Date(),
      priority: 0.9,
    },
    {
      url: `${process.env.NEXT_PUBLIC_BASE_URL}/chinh-sach-giao-hang`,
      lastModified: new Date(),
      priority: 0.9,
    },
  ];

  if (posts && posts?.item) {
    posts?.item?.forEach((e) => {
      fixPages.push({
        url: `${process.env.NEXT_PUBLIC_BASE_URL}/${e?.post_slug}`,
        lastModified: new Date(),
        priority: 0.8,
      });
    });
  }

  if (products && products?.item) {
    products?.item?.forEach((e) => {
      fixPages.push({
        url: `${process.env.NEXT_PUBLIC_BASE_URL}/san-pham/${e?.slug}`,
        lastModified: new Date(),
        priority: 0.8,
      });
    });
  }

  if (categories && categories?.length > 0) {
    categories?.forEach((e) => {
      fixPages.push({
        url: `${process.env.NEXT_PUBLIC_BASE_URL}/danh-muc-san-pham/${e?.slug}`,
        lastModified: new Date(),
        priority: 0.8,
      });
    });
  }

  return fixPages;
}
