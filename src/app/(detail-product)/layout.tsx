import Footer from '@/sections/main/components/footer/footer';
import Navbar from '@/sections/main/components/navbar/navbar';
import { postData } from '@/lib/post-data';
import { Toaster } from '@/components/ui/sonner';

const MainLayout = async ({ children }: { children: React.ReactNode }) => {
  // const session = await getServerSession(NEXT_AUTH_OPTIONS);

  const bodyGetProductHeader: any = {
    url: `wp-json/custom/v1/categories`,
    method: 'get',
  };

  // GET HISTORY SEARCH
  const bodyHistorySearch: any = {
    url: `wp-json/customer-search/v1/get-search`,
    method: 'get',
  };

  const bodyGetAcfCommon: any = {
    url: `wp-json/acf/v3/posts/4943?_fields=acf`,
    method: 'get',
  };
  const getDataListProductHeader =  postData(bodyGetProductHeader);
  const getDataAcf =  postData(bodyGetAcfCommon)
  const getDataHistorySearch = postData(bodyHistorySearch);


  const [dataListProductHeader, dataAcf, dataHistorySearch] = await Promise.all([getDataListProductHeader, getDataAcf, getDataHistorySearch])

  // END
  return (
    <div>
      <main className="max-md:mt-0 md:mb-[7.5rem]">
        <Navbar
          // avatarUser={dataGetInforUser?.avatar_url}
          // dataListCart={dataListCart}
          dataListProductHeader={dataListProductHeader}
          dataHistorySearch={dataHistorySearch}
          dataAcf={dataAcf?.acf}

        />
        <div>{children}</div>
        <Toaster />
        <Footer dataListProduct={dataListProductHeader} />
      </main>
    </div>
  );
};

export default MainLayout;
