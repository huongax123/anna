import { fetchDataRest } from '@/lib/fetch-data-rest';
import { fetchMetaData } from '@/lib/fetch-meta-data';
import { getMeta } from '@/lib/getMeta';
import { Login } from '@/sections/auth/login';
import React from 'react';

export async function generateMetadata({ params }: any) {
  const result = await fetchMetaData('tai-khoan/')
  return getMeta(result, "tai-khoan")
}

async function LoginPage() {
  const dataAcf = await fetchDataRest('GET', 'acf/v3/posts/3014/last_section');
  return <Login data={dataAcf} />;
}

export default LoginPage;
