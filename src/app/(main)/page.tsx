import { fetchMetaData } from '@/lib/fetch-meta-data';
import { getMeta } from '@/lib/getMeta';
import Home from '@/sections/home/view/home';

export async function generateMetadata({params} : any){
  const result = await fetchMetaData('')
  return getMeta(result, "")
}

function HomePage() {
  return <Home />;
}

export default HomePage;
