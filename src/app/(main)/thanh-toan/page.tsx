// eslint-disable-next-line import/no-cycle
import { fetchDataRest } from '@/lib/fetch-data-rest';
import { fetchMetaData } from '@/lib/fetch-meta-data';
import { getMeta } from '@/lib/getMeta';
import Payment from '@/sections/payment';

export async function generateMetadata({params} : any){
  const result = await fetchMetaData('thanh-toan/')
  return getMeta(result, "thanh-toan")
}

export interface IItemProvinceConvert {
  value: string;
  label: string;
}
const PaymentPage = async () => {

  const shippingMethod = await fetchDataRest(
    'GET',
    'custom/v1/shipping-methods'
  );
  return <Payment shippingData = {shippingMethod}/>;
};

export default PaymentPage;
