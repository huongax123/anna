import { fetchDataRest } from '@/lib/fetch-data-rest';
import { fetchMetaData } from '@/lib/fetch-meta-data';
import { getMeta } from '@/lib/getMeta';
import { Register } from '@/sections/auth/register';
import React from 'react';

export async function generateMetadata({params} : any){
  const result = await fetchMetaData('dang-ky/')
  return getMeta(result, "dang-ky")
}

async function RegisterPage() {
  const dataAcf = await fetchDataRest('GET', 'acf/v3/posts/3014/last_section');
  return <Register data={dataAcf} />;
}

export default RegisterPage;
