import Footer from '@/sections/main/components/footer/footer';
import Navbar from '@/sections/main/components/navbar/navbar';
import { postData } from '@/lib/post-data';
import { Toaster } from '@/components/ui/sonner';

const MainLayout = async ({ children }: { children: React.ReactNode }) => {
  const bodyGetProductHeader: any = {
    url: `wp-json/custom/v1/categories`,
    method: 'get',
  };

  const bodyGetAcfCommon: any = {
    url: `wp-json/acf/v3/posts/4943?_fields=acf`,
    method: 'get',
  };
  const bodyHistorySearch: any = {
    url: `wp-json/customer-search/v1/get-search`,
    method: 'get',
  };

  const getDataListProductHeader = postData(bodyGetProductHeader);
  const getDataHistorySearch = postData(bodyHistorySearch);
  const getDataAcf = postData(bodyGetAcfCommon);
  const [dataListProductHeader, dataAcf, dataHistorySearch] = await Promise.all(
    [getDataListProductHeader, getDataAcf, getDataHistorySearch]
  );

  return (
    <div>
      <main className="max-md:mt-0">
        <Navbar
          dataListProductHeader={dataListProductHeader}
          dataHistorySearch={dataHistorySearch}
          dataAcf={dataAcf?.acf}
        />
        <div>{children}</div>
        <Toaster />
        <Footer dataListProduct={dataListProductHeader} />
      </main>
    </div>
  );
};

export default MainLayout;
