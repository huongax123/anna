import React from 'react';
import CheckingOrder from '@/sections/checking-order';
import { fetchMetaData } from '@/lib/fetch-meta-data';
import { getMeta } from '@/lib/getMeta';


export async function generateMetadata({params} : any){
  const result = await fetchMetaData('order-checking/')
  return getMeta(result, "order-checking")
}

function CheckingOrderPage() {
  return <CheckingOrder />;
}

export default CheckingOrderPage;
