import ListProduct from '@/sections/list-product';
import { postData } from '@/lib/post-data';
import { fetchDataRest } from '@/lib/fetch-data-rest';
import { fetchMetaData } from '@/lib/fetch-meta-data';
import { getMeta } from '@/lib/getMeta';

export async function generateMetadata({ params }: any) {
  const result = await fetchMetaData('cua-hang/');
  return getMeta(result, 'cua-hang');
}
const ListProductPage = async ({
  searchParams,
  params: { slug, query },
}: any) => {
  const bodyGetlistAttribute: any = {
    url: `wp-json/custom/v1/attributes`,
    method: 'get',
  };
  const bodyGetListProduct: any = {
    url: `/wp-json/product/v1/products/filter?per_page=24&page=1`,
    method: 'get',
  };
  const getDataListAttribute = postData(bodyGetlistAttribute);
  const getDataListProductInit = postData(bodyGetListProduct);
  const getListBrand = fetchDataRest('GET', 'custom/v1/attribute-brand');
  const getListMaterial = fetchDataRest('GET', 'custom/v1/attribute-material');
  const getListShape = fetchDataRest('GET', 'custom/v1/attribute-shape');
  const getListFeature = fetchDataRest('GET', 'custom/v1/attribute-feature');
  const getDataAcf = fetchDataRest('GET', 'acf/v3/posts/9?_fields=acf');
  const getDataBanner = fetchDataRest('GET', `custom/v1/products-by-category?category=${slug}&per_page=24&page=1`);
  const [
    dataListAttribute,
    dataListProductInit,
    listBrand,
    listMaterial,
    listShape,
    listFeature,
    dataAcf,
    dataBanner,
  ] = await Promise.all([
    getDataListAttribute,
    getDataListProductInit,
    getListBrand,
    getListMaterial,
    getListShape,
    getListFeature,
    getDataAcf,
    getDataBanner,
  ]);

  return (
    <ListProduct
      searchParams={searchParams}
      dataListAttribute={dataListAttribute}
      dataListProductInit={dataListProductInit}
      listAttributeNew={[listBrand, listMaterial, listShape, listFeature]}
      dataAcf={dataAcf?.acf}
      dataBanner={dataBanner}
    />
  );
};

export default ListProductPage;
