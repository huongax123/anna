import { fetchDataRest } from '@/lib/fetch-data-rest';
import { fetchMetaData } from '@/lib/fetch-meta-data';
import { getMeta } from '@/lib/getMeta';
import SystemStore from '@/sections/system-store';
import React from 'react';

export async function generateMetadata({ params }: any) {
  const result = await fetchMetaData('he-thong-cua-hang/');
  return getMeta(result, 'he-thong-cua-hang');
}

const SystemStorePage = async () => {
  const listCityReq = fetchDataRest('GET', 'custom/v1/get-cate-location');

  const listDistReq = fetchDataRest(
    'GET',
    'custom/v1/get-cate-location-child/157'
  );
  const listSystemStoreReq = fetchDataRest(
    'GET',
    `custom/v1/get-location?tinh=${encodeURIComponent(
      'ha-noi'
    )}&huyen=${encodeURIComponent('')}`
  );

  const [listCity, listDist, listSystemStore, dataSectionHome] = await Promise.all([
    listCityReq,
    listDistReq,
    listSystemStoreReq,
    await fetchDataRest('GET', 'acf/v3/posts/3014/last_section')
  ]);
  return (
    <SystemStore
      listCity={listCity}
      listDistInit={listDist?.children}
      listSystemStoreInit={listSystemStore}
      dataSectionHome={dataSectionHome}
    />
  );
};

export default SystemStorePage;
