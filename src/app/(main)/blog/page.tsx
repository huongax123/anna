import { fetchDataRest } from '@/lib/fetch-data-rest';
import { fetchMetaData } from '@/lib/fetch-meta-data';
import { getMeta } from '@/lib/getMeta';
import Blog from '@/sections/blog';
import React from 'react';

export async function generateMetadata({ params }: any) {
  const result = await fetchMetaData('blog/');
  return getMeta(result, 'blog');
}

const BlogPage = async () => {
  // const dataInit = await fetchDataRest(
  //   'GET',
  //   'post/v1/posts?per_page=9&page=1'
  // );

  // const dataAcf = await fetchDataRest(
  //   'GET',
  //   'acf/v3/posts/3007?_fields=acf'
  // );

  // const dataSectionHome = await fetchDataRest('GET', 'acf/v3/posts/3014/last_section');

  const [dataInit, dataAcf, dataSectionHome] = await Promise.all([await fetchDataRest(
    'GET',
    'post/v1/posts?per_page=9&page=1'
  ), await fetchDataRest(
    'GET',
    'acf/v3/posts/3007?_fields=acf'
  ), await fetchDataRest('GET', 'acf/v3/posts/3014/last_section')])

  return <Blog dataInit={dataInit} dataAcf={dataAcf?.acf} dataSectionHome={dataSectionHome} />;
};

export default BlogPage;
