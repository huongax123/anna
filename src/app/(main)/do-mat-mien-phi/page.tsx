import Policy from '@/sections/policy';
import React from 'react';
import { fetchDataRest } from '@/lib/fetch-data-rest';
import { fetchMetaData } from '@/lib/fetch-meta-data';
import { getMeta } from '@/lib/getMeta';


export async function generateMetadata({ params }: any) {
  const result = await fetchMetaData('do-mat-mien-phi/')
  return getMeta(result, "do-mat-mien-phi")
}


const PolicyEyeExamPage = async () => {
  const paramApi: any = {
    method: 'get',
    urlPolicy: `wp/v2/pages?slug=do-mat-mien-phi`,
  };

  const [dataPolicyRender, dataSectionHome] = await Promise.all([await fetchDataRest(
    paramApi.method,
    paramApi.urlPolicy
  ), await fetchDataRest('GET', 'acf/v3/posts/3014/last_section')])

  return <Policy dataPolicyRender={dataPolicyRender[0]} dataSectionHome={dataSectionHome} />;
};

export default PolicyEyeExamPage;
