import { fetchDataRest } from '@/lib/fetch-data-rest';
import { fetchMetaData } from '@/lib/fetch-meta-data';
import { getMeta } from '@/lib/getMeta';
import Contact from '@/sections/contact';
import React from 'react';


export async function generateMetadata({params} : any){
  const result = await fetchMetaData('lien-he/')
  return getMeta(result, "lien-he")
}

async function ContactPage() {
  const dataSectionHome = await fetchDataRest('GET', 'acf/v3/posts/3014/last_section')
  
  return <Contact data={dataSectionHome} />;
}

export default ContactPage;
