import ProductFilter from '@/sections/product-filter';
import { fetchDataRest } from '@/lib/fetch-data-rest';
import { fetchMetaData } from '@/lib/fetch-meta-data';
import { getMeta } from '@/lib/getMeta';

export async function generateMetadata({ params }: any) {
  const result = await fetchMetaData('product-filter/');
  return getMeta(result, 'product-filter');
}

const ProductFilterPage = async ({ searchParams, params: { query } }: any) => {
  // FORMAT PARAMS
  let routerSearchCustom = '';
  for (const [keyAttribute, valueAttribute] of Object.entries(searchParams)) {
    routerSearchCustom = `${routerSearchCustom}&${keyAttribute}=${valueAttribute}`;
  }

  const bodyGetlistAttribute: any = {
    url: `wp-json/custom/v1/attributes`,
    method: 'get',
  };
  const getDataListAttribute = await fetch(
    `${process.env.NEXT_PUBLIC_REST_API}/${bodyGetlistAttribute.url}`
  ).then((res) => {
    return res.json();
  });

  const getListBrand = fetchDataRest('GET', 'custom/v1/attribute-brand');
  const getListMaterial = fetchDataRest('GET', 'custom/v1/attribute-material');
  const getListShape = fetchDataRest('GET', 'custom/v1/attribute-shape');
  const getListFeature = fetchDataRest('GET', 'custom/v1/attribute-feature');
  const getDataAcf = fetchDataRest('GET', 'acf/v3/posts/9?_fields=acf');

  const [
    listBrand,
    listMaterial,
    listShape,
    listFeature,
    dataListAttribute,
    dataAcf,
  ] = await Promise.all([
    getListBrand,
    getListMaterial,
    getListShape,
    getListFeature,
    getDataListAttribute,
    getDataAcf,
  ]);

  return (
    <ProductFilter
      routerSearchCustom={routerSearchCustom}
      listAttributeNew={[listBrand, listMaterial, listShape, listFeature]}
      dataListAttribute={dataListAttribute}
      searchParams={searchParams}
      dataAcf={dataAcf?.acf}
    />
  );
};

export default ProductFilterPage;
