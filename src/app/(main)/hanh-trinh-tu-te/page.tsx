import { fetchMetaData } from '@/lib/fetch-meta-data';
import { getMeta } from '@/lib/getMeta';
import HTTT from '@/sections/httt';
import { fetchDataRest } from '@/lib/fetch-data-rest';

export async function generateMetadata({ params }: any) {
  const result = await fetchMetaData('hanh-trinh-tu-te/');
  return getMeta(result, 'hanh-trinh-tu-te');
}

export default async function page() {
  const dataAcf = await fetchDataRest('GET', 'acf/v3/posts/3014?_fields=acf');

  return <HTTT data={dataAcf?.acf} />;
}
