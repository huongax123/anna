import Search from '@/sections/search';
import { fetchDataRest } from '@/lib/fetch-data-rest';
import { postData } from '@/lib/post-data';
import { fetchMetaData } from '@/lib/fetch-meta-data';
import { getMeta } from '@/lib/getMeta';

export async function generateMetadata({ params }: any) {
  const result = await fetchMetaData('tim-kiem/');
  return getMeta(result, 'tim-kiem');
}
const SearchPage = async ({ params: { slug }, searchParams }: any) => {
  // Get List Attribute
  const getListColor = fetchDataRest('GET', 'custom/v1/attributes');
  const getListBrand = fetchDataRest('GET', 'custom/v1/attribute-brand');
  const getListMaterial = fetchDataRest('GET', 'custom/v1/attribute-material');
  const getListShape = fetchDataRest('GET', 'custom/v1/attribute-shape');
  const getListFeature = fetchDataRest('GET', 'custom/v1/attribute-feature');

  // GET DATA SEARCH
  const getDataListSearch = postData({
    url: `wp-json/custom/v1/search/?keyword=${searchParams.search}`,
    method: 'get',
  });
  const getDataAcf = fetchDataRest('GET', 'acf/v3/posts/9?_fields=acf');

  const [
    listColor,
    listBrand,
    listMaterial,
    listShape,
    listFeature,
    dataListSearch,
    dataAcf,
  ] = await Promise.all([
    getListColor,
    getListBrand,
    getListMaterial,
    getListShape,
    getListFeature,
    getDataListSearch,
    getDataAcf,
  ]);

  return (
    <Search
      searchParams={searchParams}
      listRes={[listColor, listBrand, listMaterial, listShape, listFeature]}
      listSearchSwiper={dataListSearch}
      dataAcf={dataAcf?.acf}
    />
  );
};

export default SearchPage;
