import { postData } from '@/lib/post-data';
import { fetchDataRest } from '@/lib/fetch-data-rest';
import map from 'lodash.map';
import ListProductCategory from '@/sections/list-product-category';
import { fetchMetaData } from '@/lib/fetch-meta-data';
import { getMeta } from '@/lib/getMeta';

// meta data
export async function generateMetadata({ params: { slug } }: any) {
  const result = await fetchMetaData(`product-category/${slug}/`);
  return getMeta(result, `danh-muc-san-pham/${slug}`);
}

// Return a list of `params` to populate the [slug] dynamic segment
export async function generateStaticParams() {
  const listCategory = await fetchDataRest('GET', 'custom/v1/categories');

  return map(listCategory, (item: any, index: number) => ({
    slug: item?.slug || undefined,
  }));
}

const ListProductPage = async ({ params: { slug }, searchParams }: any) => {
  // GET LIST COLOR PRODUCT
  const bodyGetlistAttribute: any = {
    url: `wp-json/custom/v1/attributes`,
    method: 'get',
  };
  const getDataListAttribute = fetch(
    `${process.env.NEXT_PUBLIC_REST_API}/${bodyGetlistAttribute.url}`
  ).then((res) => {
    return res.json();
  });
  // end format
  // END GET LIST COLOR PRODUCT
  // Get List Attribute
  const getListBrand = fetchDataRest('GET', 'custom/v1/attribute-brand');
  const getListMaterial = fetchDataRest('GET', 'custom/v1/attribute-material');
  const getListShape = fetchDataRest('GET', 'custom/v1/attribute-shape');
  const getListFeature = fetchDataRest('GET', 'custom/v1/attribute-feature');
  const getDataAcf = fetchDataRest('GET', 'acf/v3/posts/9?_fields=acf');
  // const getDataBanner = fetchDataRest('GET', `custom/v1/products-by-category?category=${slug}&per_page=24&page=1`);
  // GET LIST PRODUCT INIT
  const bodyGetListProductByCate: any = {
    url: `wp-json/custom/v1/products-by-category?category=${slug}&per_page=24&page=1`,
    method: 'get',
  };
  const getDataListProductInit = postData(bodyGetListProductByCate);
  // END GET LIST PRODUCT INIT

  const [
    listBrand,
    listMaterial,
    listShape,
    listFeature,
    dataListAttribute,
    dataListProductInit,
    dataAcf,
    // dataBanner,
  ] = await Promise.all([
    getListBrand,
    getListMaterial,
    getListShape,
    getListFeature,
    getDataListAttribute,
    getDataListProductInit,
    getDataAcf,
    // getDataBanner,
  ]);
  return (
    <ListProductCategory
      slug={slug}
      dataListProductInit={dataListProductInit}
      dataListAttribute={dataListAttribute}
      listAttributeNew={[listBrand, listMaterial, listShape, listFeature]}
      dataAcf={dataAcf?.acf}
      // dataBanner={dataBanner}
    />
  );
};

export default ListProductPage;
