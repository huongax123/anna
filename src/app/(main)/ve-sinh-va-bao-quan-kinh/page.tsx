import Policy from '@/sections/policy';
import React from 'react';
import { fetchDataRest } from '@/lib/fetch-data-rest';
import { fetchMetaData } from '@/lib/fetch-meta-data';
import { getMeta } from '@/lib/getMeta';

export async function generateMetadata({params} : any){
  const result = await fetchMetaData('ve-sinh-va-bao-quan-kinh/')
  return getMeta(result, "ve-sinh-va-bao-quan-kinh")
}

const PolicyClearPage = async () => {
  const paramApi: any = {
    method: 'get',
    urlPolicy: `wp/v2/pages?slug=ve-sinh-va-bao-quan-kinh`,
  };

  const [dataPolicyRender, dataSectionHome] = await Promise.all([await fetchDataRest(
    paramApi.method,
    paramApi.urlPolicy
  ), await fetchDataRest('GET', 'acf/v3/posts/3014/last_section')])

  return <Policy dataPolicyRender={dataPolicyRender[0]} dataSectionHome={dataSectionHome} />;
};

export default PolicyClearPage;
