import React from 'react';
import {
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectLabel,
  SelectTrigger,
  SelectValue,
} from '@/components/ui/select';

function SortBy({ handleSort }) {
  return (
    <Select className="outline-none" onValueChange={(e) => handleSort(e)}>
      <SelectTrigger className="w-fit outline-none rounded-[0.5rem] max-md:text-[3rem] text-[1rem] font-medium p-[0.5rem] max-md:p-[2.5rem] h-fit">
        <SelectValue
          placeholder="Sort By"
          className="text-[1rem] max-md:text-[3rem] font-medium [&>span]:!text-[3rem] [&>span]:!font-medium"
        />
      </SelectTrigger>
      <SelectContent>
        <SelectGroup>
          <SelectItem
            value="none"
            className="text-[1rem] max-md:text-[3rem] font-medium max-md:p-[2rem]"
          >
            Sort By
          </SelectItem>
          <SelectItem
            value="desc"
            className="text-[1rem] max-md:text-[3rem] font-medium max-md:p-[2rem]"
          >
            Giá Cao - Thấp
          </SelectItem>
          <SelectItem
            value="asc"
            className="text-[1rem] max-md:text-[3rem] font-medium max-md:p-[2rem]"
          >
            Giá Thấp - Cao
          </SelectItem>
          <SelectItem
            value="featured"
            className="text-[1rem] max-md:text-[3rem] font-medium max-md:p-[2rem]"
          >
            Nổi bật
          </SelectItem>
        </SelectGroup>
      </SelectContent>
    </Select>
  );
}

export default SortBy;
