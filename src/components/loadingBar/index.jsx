'use client';

import React from 'react';
import { AppProgressBar as ProgressBar } from 'next-nprogress-bar';
const LoadingBarProvider = ({ children }) => {
  return (
    <>
      <ProgressBar
        color="#55D5D2"
        height="0.25rem"
        options={{ showSpinner: false }}
      />
      {children}
    </>
  );
};

export default LoadingBarProvider;
